import "./extension.js"
import utils from "./utils.js"

class CoreElement extends HTMLElement {
	constructor() {
		super()
		this._never_connected = true
		this._listeners = new Set()
		this._attribute_updating = {}
		this._initPropeties()
		this._config.use_shadow && this.attachShadow({ mode: "open" })
	}

	get _config() {
		return { use_shadow: true }
	}

	get root() {
		return this.shadowRoot || this
	}

	connectedCallback() {
		if (this._never_connected) {
			this._never_connected = false
			this.onConnectOnce()
		}
		this.onConnect()
	}

	disconnectedCallback() {
		this.onDisconnect()
	}

	// will override
	onConnect() {}

	// will override
	onConnectOnce() {}

	// will override
	onDisconnect() {}

	on(scope, type, callback, opt) {
		if (typeof type === "function") {
			opt = callback
			callback = type
			type = scope
			scope = null
		}
		const target = this.root
		const handler = eve => {
			if (scope) {
				if (typeof scope === "string") {
					const elem = eve.target.closest(scope)
					if (elem && target.contains(elem)) {
						eve.scope = elem
						callback(eve)
					}
				} else if (scope instanceof HTMLElement) {
					if (scope.contains(eve.target) && target.contains(scope)) {
						eve.scope = scope
						callback(eve)
					}
				}
			} else {
				callback(eve)
			}
		}
		target.addEventListener(type, handler, opt)
		this._listeners.add({ scope, type, callback, opt, handler })
	}

	off(type, callback) {
		for (const item of [...remove_targets]) {
			const { type, handler, opt } = item
			if (type && e.type !== type) {
				continue
			}
			if (callback && e.callback !== callback) {
				continue
			}
			const target = this.root
			target.removeEventListener(type, handler, opt)
			this._listeners.delete(item)
		}
	}

	dispatch(type, detail, option = {}, target = this) {
		target.dispatchEvent(new CustomEvent(type, { ...option, detail }))
	}

	$(selector, root) {
		return (root || this.root).querySelector(selector)
	}

	$$(selector, root) {
		return [...(root || this.root).querySelectorAll(selector)]
	}

	attributeChangedCallback(name, old_value, new_value) {
		if (this.properties[name]._updating) return

		// Update using setter when attribute is changed directly.
		// Skip if same value
		if (old_value !== new_value) {
			this[name] = new_value
		}
	}

	static get observedAttributes() {
		return Object.entries(this.properties)
			.filter(([key, value]) => ["boolean", "string", "number"].includes(value.attribute))
			.map(e => e[0])
	}

	setAttr(name, value) {
		if (typeof value === "boolean" || value == null) {
			value ? this.setAttribute(name, "") : this.removeAttribute(name)
		} else {
			this.setAttribute(name, value)
		}
	}

	_initPropeties() {
		this.properties = this.constructor.properties
		for (const [key, option] of this.properties) {
			Object.defineProperty(this, key, {
				get() {
					let value
					if (option.attribute === "boolean") {
						value = this.hasAttribute(key)
					} else if (option.attribute === "string") {
						value = this.hasAttribute(key) ? this.getAttribute(key) : option.default
					} else if (option.attribute === "number") {
						value = this.hasAttribute(key) ? +this.getAttribute(key) : option.default
					} else {
						value = option.value
					}

					if (typeof option.get === "function") {
						return option.get(value)
					} else {
						return value
					}
				},
				set(value) {
					if (typeof option.before === "function") {
						const obj = option.before(value)
						if (obj && "value" in obj) {
							value = obj.value
						}
					}

					option._updating = true
					if (option.attribute === "boolean") {
						this.setAttr(key, !!value)
					} else if (option.attribute === "string") {
						this.setAttr(key, String(value))
					} else if (option.attribute === "number") {
						this.setAttr(key, +value)
					} else {
						option.value = value
					}
					option._updating = false

					if (typeof option.after === "function") {
						option.after(value)
					}
					if (option.update) {
						this.update(key)
					}
				},
			})
		}
	}

	// will override
	static get properties() {
		return {}
	}

	// will override
	update(key) {}
}

export { CoreElement }

/*
properties

{
	key: {
		attribute: "string", // boolean, string, number, null
		// attribute が null のとき
		value: [],
		// attribute が string, number のとき boolean は false 固定
		default: "a"
		// settter 処理の前に実行する
		// 返り値は {value: 100} 形式なら値を置き換える
		before: () => {},
		// setter 処理の後に実行する
		after: () => {},
		// 値変更時に更新処理が必要か
		// true なら update メソッドが呼び出される
		update: true,
		// get 時の処理
		get: () => {}
		// 内部的に更新中を表す
		_updating: true
	}
}
*/
