import { CoreElement } from "./core-element.js"

/*
<dialog-host>
	<div>aa</div>
	<div>bb</div>
</dialog-host>
*/

class DialogHost extends CoreElement {
	onConnectOnce() {
		this.shadowRoot.innerHTML = `
			<style>
				:host {
					--bg: #0008;
					--fg: #fff;
				}
				
				#root {
					display: flex;
					justify-content: center;
					align-items: center;
					background: var(--bg);
					position: fixed;
					left: 0;
					top: 0;
					width: 100vw;
					height: 100vh;
				}

				:host([hidden]){
					display: none;
				}

				#content {
					margin: auto;
					background: var(--fg);
				}
			</style>
			<div id="root">
				<div id="content"><slot></div>
			</div>
		`

		this.hidden = true

		this.on("click", eve => {
			if (eve.target.id === "root") {
				this.hidden = true
			}
		})
	}

	show() {
		this.hidden = false
	}

	hide() {
		this.hidden = true
	}
}

window.customElements.define("dialog-host", DialogHost)
