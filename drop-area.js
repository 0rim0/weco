import { CoreElement } from "./core-element.js"
import utils from "./utils.js"

/*
<drop-area>
	<div>aa</div>
	<div>bb</div>
</drop-area>
*/

class DropArea extends CoreElement {
	onConnectOnce() {
		this.shadowRoot.innerHTML = `
			<style>
				:host {
					display: block;
					position: relative;
					display: block;
				}

				#catcher {
					display: flex;
					justify-content: center;
					align-items: center;
					position: absolute;
					left: 0;
					top: 0;
					width: 100%;
					height: 100%;
					z-index: 10;
					background: #000a;
					color: white;
					outline: 2px dashed white;
					outline-offset: -5px;
				}

				#catcher:not(.active) {
					display: none;
				}
			</style>
			<div id="catcher"></div>
			<slot>
		`
		const catcher = this.$("#catcher")
		catcher.textContent = this.message

		// this.on の代わりにターゲットを直接指定できる utils.on を使う
		// shadowRoot につけると dragenter が全体に扱いにならない

		utils.on(this, "dragenter", eve => {
			catcher.classList.add("active")
		})

		utils.on(catcher, "dragleave", eve => {
			catcher.classList.remove("active")
		})

		utils.on(catcher, "dragover", eve => eve.preventDefault())
		utils.on(catcher, "drop", eve => {
			eve.preventDefault()
			catcher.classList.remove("active")
			const files = eve.dataTransfer.files
			files.length && this.dispatch("file-drop", [...files], { bubbles: true })
		})
	}

	static get properties() {
		return {
			message: {
				attribute: "string",
				default: "Drop Here!",
			},
		}
	}
}

window.customElements.define("drop-area", DropArea)
