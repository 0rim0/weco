// object を for-of 可能にする
Object.prototype[Symbol.iterator] = function*() {
	yield* Object.entries(this)
}
