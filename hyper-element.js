import { CoreElement } from "./core-element.js"
import { bind, wire } from "https://cdn.jsdelivr.net/npm/hyperhtml@2.12.0/esm.js"

class HyperElement extends CoreElement {
	constructor() {
		super()
		this.listeners = this.constructor.listeners.call(this)
		this._render = bind(this.root)
	}

	onConnectOnce() {
		this.update()
	}

	// will override
	render() {
		this._render``
	}

	update(key) {
		this.render()
	}

	// will override
	static listeners() {
		return {}
	}
}

export { HyperElement }

/*

listeners が返す関数は bind 済みのものにする

*/
