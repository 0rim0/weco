import { HyperElement } from "./hyper-element.js"

class HyperExample extends HyperElement {
	onConnect() {
		super.onConnect()
		this.timer = setInterval(() => {
			this.now = new Date()
		}, 1000)
	}

	onDisconnect() {
		super.onDisconnect()
		clearInterval(this.timer)
	}

	render(value) {
		this._render`
			<h3>hyper example</h3>
			<div>num: ${this.num} <button onclick=${this.listeners.up}>+</button></div>
			<div>now: ${this.now}</div>
		`
	}

	static get properties() {
		return {
			num: {
				attribute: "number",
				default: 10,
				update: true,
			},
			now: {
				value: new Date().toLocaleString(),
				before(value) {
					return { value: new Date(value).toLocaleString() }
				},
				update: true,
			},
		}
	}

	static listeners() {
		return {
			up: eve => this.num++,
		}
	}
}

window.customElements.define("hyper-example", HyperExample)
