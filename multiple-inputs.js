import { CoreElement } from "./core-element.js"

class MultipleInputs extends CoreElement {
	onConnectOnce() {
		this.on("input", "input", eve => {
			this.onInput(eve.target)
		})
		this.on("button", "click", eve => {
			this.onClickDelete(eve.target)
		})
		this.init()
	}

	init() {
		this.virtual = this.create()
		this.virtual.id = "virtual"
		this.shadowRoot.innerHTML = `
			<style>
				#virtual button {
					display: none;
				}
			
				#container.min button {
					display: none;
				}
			
				#container.max #virtual {
					display: none;
				}
			
				#virtual input {
					background: #fbfbcb;
				}
			
				button {
					background: crimson;
					color: white;
					border: 1px solid #880000;
					border-radius: 3px;
				}
			</style>
			<div id="container"></div>
		`
		this.container = this.$("#container")
		this.container.append(this.virtual)

		// 最低限必要な数の input を用意する
		for (let i = 0, l = this.mincount; i < l; i++) {
			this.addInput()
		}

		this.updateState()
	}

	updateState() {
		// virtual 以外の数
		const count = this.container.childElementCount - 1
		const max = this.maxcount
		const min = this.mincount

		this.container.classList.toggle("max", count >= max)
		this.container.classList.toggle("min", count <= min)
	}

	onInput(input) {
		if (this.virtual.contains(input)) {
			const elem = this.addInput()
			const new_input = this.$("input", elem)
			new_input.value = input.value
			input.value = ""
			new_input.focus()
		} else if (input.value === "" && this.autoremove && !this.container.classList.contains("min")) {
			this.removeInput(input)
		}
		this.updateState()
	}

	onClickDelete(btn) {
		this.removeInput(btn)
	}

	addInput() {
		const elem = this.create()
		this.virtual.before(elem)
		return elem
	}

	removeInput(elem) {
		const div = elem.closest("#container>div")
		div && div.remove()
		this.updateState()
	}

	create() {
		const div = document.createElement("div")
		div.innerHTML = `
			<input maxlength="${this.maxlength}">
			<button>×</button>
		`
		return div
	}

	static get properties() {
		return {
			mincount: {
				attribute: "number",
				default: 0,
				get(value) {
					return Math.max(0, value)
				},
			},
			maxcount: {
				attribute: "number",
				default: 10,
				get(value) {
					return Math.max(1, value)
				},
			},
			maxlength: {
				attribute: "number",
				default: 100,
				get(value) {
					return Math.max(1, value)
				},
			},
			autoremove: {
				attribute: "boolean",
			},
		}
	}

	get values() {
		return this.$$("input")
			.map(e => e.value)
			.filter(e => e)
	}
	set values(value) {
		if (Array.isArray(value)) {
			const n = Math.min(this.maxcount, value.length)
			this.addInput()
			let elem = this.container.firstElementChild
			for (const str of value.slice(0, this.maxcount)) {
				this.$("input", elem).value = str
				if (elem.nextElementSibling === this.virtual) {
					this.addInput()
				}
				elem = elem.nextElementSibling
			}
			while (elem.nextElementSibling !== this.virtual) {
				elem.nextElementSibling.remove()
			}
			elem.remove()
			this.updateState()
		}
	}

	checkValidity() {
		return this.values.length >= this.mincount && this.values.length <= this.maxcount
	}
}

window.customElements.define("multiple-inputs", MultipleInputs)
