import { CoreElement } from "./core-element.js"
import utils from "./utils.js"

/*
<page-switcher>
	<button data-component="example-component1">aa</button>
	<button data-component="example-component2">bb</button>
</page-switcher>
*/

class PageSwitcher extends CoreElement {
	onConnectOnce() {
		this.shadowRoot.innerHTML = `
			<style>
				:host {
					display: block;
				}
			</style>
			<header id="header"><slot></header>
			<section id="content"></section>
		`

		this.on("click", eve => {
			if (this.contains(eve.target)) {
				const btn = eve.target.closest("[data-component]")
				if (btn) {
					this.switch(btn.dataset.component)
				}
			}
		})

		this.switch(this.component || 0)
	}

	static get properties() {
		return {
			component: {
				attribute: "string",
				update: true,
			},
		}
	}

	switch(name) {
		if (typeof name === "number") {
			const elem = this.$$("[data-component]", this)[name]
			name = elem ? elem.dataset.component : name
		}
		this.component = name
	}

	update() {
		const name = this.component
		const old_active = this.$("[data-component][data-ps-active]", this)
		const new_active = this.$(utils.esc`[data-component="${name}"]`, this)
		if (old_active !== new_active) {
			old_active && old_active.removeAttribute("data-ps-active")
			new_active && new_active.setAttribute("data-ps-active", "")
			const content = this.$("#content")
			content.innerHTML = ""
			content.append(document.createElement(name))
		}
	}
}

window.customElements.define("page-switcher", PageSwitcher)
