import { CoreElement } from "./core-element.js"

class RerenderElement extends CoreElement {
	onConnectOnce(){
		this.update()
	}

	// will override
	template(){
		return ``
	}

	update(key) {
		this.root.innerHTML = this.template()
	}
}

export { RerenderElement }

/*

毎回全部を書き換える
特に困るほどでもない
ただし毎秒更新とかあると
操作中に書き換えられる
→入力中データも保存必要
それでもドラッグ操作とか
ボタン長押しとかはできない

*/
