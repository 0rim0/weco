import { RerenderElement } from "./rerender-element.js"

class RerenderExample extends RerenderElement {
	onConnect() {
		super.onConnect()
		this.timer = setInterval(() => {
			this.now = new Date()
		}, 1000)
	}

	onConnectOnce() {
		super.onConnectOnce()
		this.on("button", "click", eve => this.num++)
	}

	onDisconnect() {
		super.onDisconnect()
		clearInterval(this.timer)
	}

	template() {
		return `
			<h3>rerender example</h3>
			<div>num: ${this.num} <button>+</button></div>
			<div>now: ${this.now}</div>
		`
	}

	static get properties() {
		return {
			num: {
				attribute: "number",
				default: 10,
				update: true,
			},
			now: {
				value: new Date().toLocaleString(),
				before(value) {
					return { value: new Date(value).toLocaleString() }
				},
				update: true,
			},
		}
	}
}

window.customElements.define("rerender-example", RerenderExample)
