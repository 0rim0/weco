import { CoreElement } from "./core-element.js"

class SortableList extends CoreElement {
	onConnectOnce() {
		this.shadowRoot.innerHTML = `
			<style>
				:host {
					display: block;
				}
			</style>
			<div id="items">
				<slot/>
			</div>
		`

		this.on("dragstart", eve => {
			const target = eve.target
			if (target.parentElement === this) {
				eve.dataTransfer.effectAllowed = "move"
				target.classList.add("dragging")
				this.dragging = target
				this._children = [...this.children]
			}
		})

		this.on("dragenter", eve => {
			const target = eve.target

			const entering = target.closest("sortable-list>*")
			const dragging = this.dragging
			if (entering && dragging && dragging !== entering) {
				let dragging_index, entering_index
				let index = 0
				for (const child of this.children) {
					if (child === entering) {
						entering_index = index
					}
					if (child === dragging) {
						dragging_index = index
					}
					if (dragging_index != null && entering_index != null) {
						break
					}
					index++
				}

				if (entering_index < dragging_index) {
					entering.before(dragging)
				} else {
					entering.after(dragging)
				}
			}
		})

		this.on("dragover", eve => {
			const target = eve.target

			if (target.closest("sortable-list>*") && this.dragging) {
				eve.preventDefault()
				eve.dataTransfer.dropEffect = "move"
			}
		})

		this.on("drop", eve => {
			const target = eve.target
			if (target.closest("sortable-list>*") && this.dragging) {
				eve.preventDefault()
				this._children = null
			}
		})

		this.on("dragend", eve => {
			const target = eve.target
			if (target.parentElement === this && this.dragging) {
				target.classList.remove("dragging")
				this.dragging = null
				if (this._children) {
					this.append(...this._children)
				}
				this._children = null
			}
		})
	}
}

window.customElements.define("sortable-list", SortableList)
