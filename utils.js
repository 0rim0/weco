import "./extension.js"

export default {
	on(target, scope, type, callback, opt) {
		if (typeof type === "function") {
			opt = callback
			callback = type
			type = scope
			scope = null
		}
		const handler = eve => {
			if (scope) {
				if (typeof scope === "string") {
					const elem = eve.target.closest(scope)
					if (elem && target.contains(elem)) {
						eve.scope = elem
						callback(eve)
					}
				} else if (scope instanceof HTMLElement) {
					if (scope.contains(eve.target) && target.contains(scope)) {
						eve.scope = scope
						callback(eve)
					}
				}
			} else {
				callback(eve)
			}
		}
		target.addEventListener(type, handler, opt)
		return handler
	},

	escape(text) {
		const div = document.createElement("div")
		div.textContent = text
		return div.innerHTML
	},

	/// タグ形式の文字列と値を文字列化する
	buildTagString(strs, ...values) {
		let str = strs[0]
		for (let i = 1, l = strs.length; i < l; i++) {
			str += values[i - 1] + strs[i]
		}
		return str
	},

	/// html escape して埋め込み
	/// {html: "<br>"} 形式のオブジェクトの場合はエスケープせずそのまま
	esc(strs, ...values) {
		return this.buildTagString(
			strs,
			...values.map(e => {
				if (e instanceof Object) {
					return e.html
				} else {
					return this.escape(e)
				}
			})
		)
	},

	html2fragment(html) {
		const tpl = document.createElement("template")
		tpl.innerHTML = html
		return tpl.content
	},

	/// 指定ミリ秒待機して解決する Promise を返す
	async wait(msec) {
		return new Promise((ok, ng) => setTimeout(ok, msec))
	},

	regexp: {
		escape(str) {
			return str.replace(/[-\/\\^$*+?.()|[\]{}]/g, "\\$&")
		},
	},

	object: {
		from(entries, mapper) {
			if (!Array.isArray(entries)) {
				entries = Object.entries(entries)
			}
			if (typeof mapper === "function") {
				entries = entries.map(mapper)
			}
			const result = {}
			for (const [key, value] of entries) {
				result[key] = value
			}
			return result
		},
	},
}
